package com.exalt;

import java.util.ArrayList;
import java.util.List;

final class StringCalculator {
	/**
	 * The method can add an unknown amount of numbers, separated by commas or new
	 * lines or a defined new delimiter, and will return their sum. For example ""
	 * or "1", "1,2", "1\n2,3", or "//;\n1;2" as inputs.
	 * 
	 * To change a delimiter, the beginning of the string will contain a separate
	 * line that looks like this: "//[delimiter]\n[numbers…]"
	 * 
	 * For an empty string it will return 0.
	 * 
	 * Negative numbers are not allowed in the input.
	 * 
	 * @param numbers String containing the numbers to add, separated by commas.
	 * @return the sum of the numbers in the numbers String
	 * @throws NotOkException When the input is not well formatted
	 * @throws NegativeNumbersNotAllowedException  When the input contains negative numbers
	 */
	static final int Add(final String numbers) throws NotOkException, NegativeNumbersNotAllowedException {
		String updatedDelimitersNumbers = numbers;
		String delimiter = ",";
		String newLineDelimiter = "\n";
		int result = 0;
		final List<String> negativeNumbers = new ArrayList<>();

		if (numbers.isEmpty()) {
			return 0;
		}

		/*
		 * Is Delimiter redefined ?
		 */
		if (numbers.matches("^//.{1}(\n.*)*")) {
			updatedDelimitersNumbers = numbers.substring(4);
			delimiter = numbers.substring(2, 3);
		}

		/*
		 * Is input well formatted?
		 */
		if (numbers.endsWith(delimiter + newLineDelimiter)) {
			throw new NotOkException("Input String must not end with new line separator");
		}

		/*
		 * Sum the string content.
		 */
		String[] numbersArray = updatedDelimitersNumbers.split(delimiter + "|" + newLineDelimiter);
		for (String number : numbersArray) {
			int numberAsInt = Integer.valueOf(number);
			if(numberAsInt < 0) {
				negativeNumbers.add(number);
			}
			result += numberAsInt;
		}

		/*
		 * Negative numbers are not allowed.
		 */
		if(!negativeNumbers.isEmpty()) {
			throw new NegativeNumbersNotAllowedException("negatives not allowed : " + String.join(",", negativeNumbers));
		}
		return result;
	}
}