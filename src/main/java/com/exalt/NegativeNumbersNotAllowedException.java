package com.exalt;

public class NegativeNumbersNotAllowedException extends Exception {
	/**
	 * Default serial version UID
	 */
	private static final long serialVersionUID = 1L;

	public NegativeNumbersNotAllowedException(String errorMessage) {
		super(errorMessage);
	}

}
