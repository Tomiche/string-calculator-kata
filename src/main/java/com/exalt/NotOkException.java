package com.exalt;

/**
 * Exception that will be thrown if the input string is not OK.
 * 
 * @author Tomiche
 *
 */
public class NotOkException extends Exception {
	/**
	 * Default serial version UID
	 */
	private static final long serialVersionUID = 1L;

	public NotOkException(String errorMessage) {
		super(errorMessage);
	}
}
