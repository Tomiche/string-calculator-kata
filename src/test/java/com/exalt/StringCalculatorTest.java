package com.exalt;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static com.exalt.StringCalculator.Add;
import org.junit.jupiter.api.Test;

public class StringCalculatorTest {
	private static final String DEFAULT_MESSAGE = "negatives not allowed : ";

	/**
	 * Step 1 The method can take up to two numbers, separated by commas, and will
	 * return their sum. For example “” or “1” or “1,2” as inputs.
	 * 
	 * For an empty string it will return 0.
	 */

	@Test
	void add_empty_string_should_return_0() throws NotOkException, NegativeNumbersNotAllowedException {
		int value = Add("");
		assertEquals(0, value);
	}

	@Test
	void add_single_positive_number_should_return_this_number() throws NotOkException, NegativeNumbersNotAllowedException {
		assertEquals(1, Add("1"));
	}

	@Test
	void add_single_negative_number_should_throw_an_exception() throws NotOkException, NegativeNumbersNotAllowedException {
		NegativeNumbersNotAllowedException exception = assertThrows(NegativeNumbersNotAllowedException.class, () -> Add("-1"));
		assertEquals(DEFAULT_MESSAGE + "-1", exception.getMessage());
	}

	@Test
	void add_single_zero_number_should_return_this_number() throws NotOkException, NegativeNumbersNotAllowedException {
		assertEquals(0, Add("0"));
	}

	@Test
	void add_dual_positive_values_should_return_the_sum_of_these_numbers() throws NotOkException, NegativeNumbersNotAllowedException {
		assertEquals(3, Add("1,2"));
	}

	@Test
	void add_dual_negative_values_should_throw_an_exception() throws NotOkException, NegativeNumbersNotAllowedException {
		NegativeNumbersNotAllowedException exception = assertThrows(NegativeNumbersNotAllowedException.class, () -> Add("-1,-2"));
		assertEquals(DEFAULT_MESSAGE + "-1,-2", exception.getMessage());
	}

	@Test
	void add_dual_negative_and_positive_values_should_throw_an_exception() throws NotOkException, NegativeNumbersNotAllowedException {
		NegativeNumbersNotAllowedException exception = assertThrows(NegativeNumbersNotAllowedException.class, () -> Add("-1,2"));
		assertEquals(DEFAULT_MESSAGE + "-1", exception.getMessage());
	}

	@Test
	void add_dual_positive_and_negative_values_should_throw_an_exception() throws NotOkException, NegativeNumbersNotAllowedException {
		NegativeNumbersNotAllowedException exception = assertThrows(NegativeNumbersNotAllowedException.class, () -> Add("1,-2"));
		assertEquals(DEFAULT_MESSAGE + "-2", exception.getMessage());
	}

	/**
	 * Step 2 : Step 1 + Allow the Add method to handle an unknown amount of
	 * numbers.
	 */
	@Test
	void add_three_postive_values_should_return_the_sum_of_these_numbers() throws NotOkException, NegativeNumbersNotAllowedException {
		assertEquals(3, Add("1,1,1"));
	}

	@Test
	void add_three_negative_values_should_throw_an_exception() throws NotOkException, NegativeNumbersNotAllowedException {
		NegativeNumbersNotAllowedException exception = assertThrows(NegativeNumbersNotAllowedException.class, () -> Add("-1,-1,-1"));
		assertEquals(DEFAULT_MESSAGE + "-1,-1,-1", exception.getMessage());
	}

	@Test
	void add_three_mixed_values_should_throw_an_exception() throws NotOkException, NegativeNumbersNotAllowedException {
		NegativeNumbersNotAllowedException exception = assertThrows(NegativeNumbersNotAllowedException.class, () -> Add("-1,4,-1"));
		assertEquals(DEFAULT_MESSAGE + "-1,-1", exception.getMessage());
	}

	@Test
	void add_random_values_should_return_the_sum_of_these_values() throws NotOkException, NegativeNumbersNotAllowedException {
		int iterationNumber = (int) (Math.random() * ((1000) + 1));

		StringBuilder stringToSum = new StringBuilder();
		for (int i = 0; i < iterationNumber; i++) {
			stringToSum.append("1,");
		}
		String sumToCheck = stringToSum.toString().substring(0, stringToSum.length());
		assertEquals(iterationNumber, Add(sumToCheck));
	}

	/**
	 * Step 3 : Step 2 + Allow the Add method to handle new lines between numbers
	 * (instead of commas):
	 * 
	 * The following input is ok: "1\n2,3" (will equal 6)
	 * 
	 * The following input is NOT ok: "1,\n" (not need to prove it - just
	 * clarifying)
	 */
	@Test
	void add_positive_values_with_new_line_separators_should_return_the_sum_of_these_values() throws NotOkException, NegativeNumbersNotAllowedException {
		assertEquals(6, Add("1\n2,3"));
	}

	@Test
	void add_with_new_line_separator_in_the_end_is_not_ok() {
		assertThrows(NotOkException.class, () -> Add("1,\n"));
	}

	/**
	 * Step 4 : Step 3 + Support different delimiters:
	 * 
	 * To change a delimiter, the beginning of the string will contain a separate
	 * line that looks like this: "//[delimiter]\n[numbers…]" for example "//;\n1;2"
	 * should return three where the default delimiter is ";".
	 * 
	 * The first line is optional. All existing scenarios should still be supported.
	 * 
	 */

	@Test
	void add_method_should_enable_delimiter_change_to_semicolon() throws NotOkException, NegativeNumbersNotAllowedException {
		assertEquals(3, Add("//;\n1;2"));
	}

	@Test
	void add_method_should_enable_delimiter_change_to_sharp() throws NotOkException, NegativeNumbersNotAllowedException {
		assertEquals(3, Add("//#\n1#2"));
	}

	@Test
	void add_method_should_enable_delimiter_change_to_sharp_and_let_new_line_be_well_interpreted()
			throws NotOkException, NegativeNumbersNotAllowedException {
		assertEquals(6, Add("//#\n3\n1#2"));
	}

	@Test
	void add_method_should_enable_delimiter_change_to_sharp_and_let_new_lines_be_well_interpreted()
			throws NotOkException, NegativeNumbersNotAllowedException {
		assertEquals(9, Add("//#\n3\n1#2\n1#2"));
	}

	/**
	 * Step 5 : Step 4 + Calling Add with a negative number will throw an exception
	 * "negatives not allowed" - and the negative that was passed.
	 * 
	 * If there are multiple negatives, show all of them in the exception message.
	 */
	
	@Test
	void negative_numbers_on_multiple_lines_should_return_an_exception() {
		NegativeNumbersNotAllowedException exception = assertThrows(NegativeNumbersNotAllowedException.class, () -> Add("//#\n-3\n1#-2"));
		assertEquals(DEFAULT_MESSAGE + "-3,-2", exception.getMessage());
	}
}
